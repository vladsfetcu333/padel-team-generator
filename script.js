function generateAmericanoPadelTournament(players) {
    const n = players.length;
    if (n < 4 || n > 5) {
        return "The tournament requires 4 or 5 players.";
    }

    let matches = [];
    if (n === 4) {
        matches = [
            [[players[0], players[1]], [players[2], players[3]]],
            [[players[0], players[2]], [players[1], players[3]]],
            [[players[0], players[3]], [players[1], players[2]]]
        ];
    } else if (n === 5) {
        matches = [
            [[players[0], players[1]], [players[2], players[3]], players[4]],
            [[players[0], players[2]], [players[1], players[4]], players[3]],
            [[players[0], players[3]], [players[1], players[2]], players[4]],
            [[players[0], players[4]], [players[2], players[3]], players[1]],
            [[players[1], players[3]], [players[2], players[4]], players[0]]
        ];
    }

    let output = "Tournament Matches:\n";
    matches.forEach((match, index) => {
        output += `Match ${index + 1}:\n`;
        match.forEach(pair => {
            if (Array.isArray(pair)) {
                output += `${pair[0]} & ${pair[1]}\n`;
            } else {
                output += `${pair} is sitting out\n`;
            }
        });
        output += "\n";
    });
    return output;
}

function generateTournament() {
    const players = [];
    for (let i = 1; i <= 5; i++) {
        const player = document.getElementById(`player${i}`).value.trim();
        if (player) {
            players.push(player);
        }
    }

    const output = generateAmericanoPadelTournament(players);
    document.getElementById("output").innerText = output;
}
